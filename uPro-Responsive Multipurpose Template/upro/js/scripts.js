(function() {
    "use strict";
    $('#loader').delay(700).fadeOut();
    $('#mask').delay(1200).fadeOut("slow");

    // Global DOM elements
    window.upro = {
        ele: {}
    };
    upro.ele['win'] = $(window);
    upro.ele['doc'] = $(document);

    // Menu Top
    $('#main-nav').affix({
        offset: {
            top: 100
        }
    })


    // Slider Home
    $('.mainslide img').each(function() {
        var $src = $(this).attr('src');
        $(this).parent().css({
            'background': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), rgba(0, 0, 0, 0.4) url(' + $src + ')',
            'background-position': 'center center',
            'background-repeat': 'no-repeat',
            'background-attachment': 'fixed',
            'background-size': 'cover',
        });
        $(this).remove();
    });

    // Pages Image
    $('#intro-page img').each(function() {
        var $src = $(this).attr('src');
        $(this).parent().css({
            'background': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), rgba(0, 0, 0, 0.4) url(' + $src + ')',
            'background-position': 'center center',
            'background-repeat': 'no-repeat',
            'background-attachment': 'fixed',
            'background-size': 'cover',
        });
        $(this).remove();
    });

    // Intro section Type
    $("#typed").typed({
        stringsElement: $('#typed-strings'),
        typeSpeed: 100,
        backDelay: 1000,
        loop: true,
        cursorChar: "|",
    });


	// Animation Load
    $('.animated').appear(function() {
        var element = $(this);
        var animation = element.data('animation');
        var animationDelay = element.data('delay');
        if (animationDelay) {
            setTimeout(function() {
                element.addClass(animation + " visible");
                element.removeClass('hiding');
            }, animationDelay);
        } else {
            element.addClass(animation + " visible");
            element.removeClass('hiding');
        }

    }, {
        accY: -150
    });


    if (document.getElementsByClassName("team-item").length) {
        var eventFired = false,
            objectPositionTop = $('.team-item').offset().top;
    }
    upro.ele['win'].on('scroll', function() {
        // Counter On Scroll
        var currentPosition = upro.ele['doc'].scrollTop();
        if (currentPosition > objectPositionTop && eventFired === false) {
            eventFired = true;
            $('.counter').each(function() {
                $(this).prop('count', 100).animate({
                    count: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }
        // Back to Top On Scroll
        if ($(this).scrollTop() > 100) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    $('#back-to-top').on('click', function() {
        $('html, body').animate({
            scrollTop: 0,
            easing: 'swing'
        }, 750);
        return false;
    });

	// Contact Form
   $('.contact-form').on('submit', function(e) {
        e.preventDefault();
        $.post('mail/send.php', $(this).serialize()).done(function(data) {
            $('.contact-us').fadeOut('slow', function() {
                $('.contact-us').fadeIn('slow').html(data);
            });
        }).fail(function() {
            alert('SOMETHING WENT WRONG! PLEASE TRY AGAIN.');
        });
    });

})();